## Lonely-dobble
Lonely-dobble is a terminal based single player game. The game is about cognitive skills, how fast a player perceives a character and presses the key. It is a good practice for typing. Lonely dobble is inspired by [Dobble card game](https://www.dobblegame.co.uk/)

### How to play the game
When the game starts the user is presented with a welcome screen.And then two sets of 5 characters each are displayed. One character is present on both sets. When the user presses the key that corresponds to the common character, two sets of new characters are displayed and so on. It works in vim and terminal. Press Escape to exit the game.

TODO:
- [x] Press escape to exit the game 
- [x] Display time elapsed 
- [x] Wrap welcome screen text
- [x] Center welcome text
- [x] Display all time average
- [ ] Display good bye message after player presses escape key
- [ ] Display number of games played

## Installation
1. Clone the repository
```
$ git clone git@gitlab.com:jewiet/lonely-dobble.git
```
2. Run the program
```
$ lein run
```

## Examples
```
juswr
udeza

user=> u

Time: 1456 milliseconds
Average-time: 1456 milliseconds


xorpm
ghqrl

user=> p
user=> r  

Time: 4567 milliseconds
Averagae-time: 3011.5 milliseconds

cvyua
bgcmw

user=> c 

Time: 2532 milliseconds
Average-time: 2851.67 milliseconds
```

## Motivation
I created lonely-dobble to learn clojure programming language.

## Thanks to:
[tad-lispy](https://gitlab.com/tad-lispy) for mentoring and reviewing my code

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
