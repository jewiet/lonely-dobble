(ns lonely-dobble.card-generator
  (:require [clojure.string  :as str])
  (:use [clojure.set :only (difference)])
  (:gen-class))


(def symbols (set "abcdefghijklmnopqrstuvwxyz"))

(defn take-random [n symbols]
  (take n (shuffle symbols)))

(def card-1 (take-random 5 symbols))


(def common-symbol (rand-nth card-1))

(def card-2 (->> card-1
                 (difference symbols)
                 (take-random 5)
                 (cons common-symbol)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (shuffle card-1))
  (println (shuffle card-2)))

