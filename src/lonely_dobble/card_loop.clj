(ns lonely-dobble.card-loop
  (:require [lanterna.screen])
  (:require [lanterna.constants :as constants])
  (:require [clojure.string  :as str])
  (:use [clojure.set :only (difference)])
  (:gen-class))


(def screen (lanterna.screen/get-screen :text))

(def symbols (set "abcdefghijklmnopqrstuvwxyz"))

(defn take-random [n symbols]
  (take n (shuffle  symbols)))

(def card-1 (take-random 5 symbols))


(def common-symbol (rand-nth card-1))

(def card-2 (->> card-1
                 (difference symbols)
                 (take-random 4)
                 (cons common-symbol)))
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (lanterna.screen/start screen)
  (let [size (lanterna.screen/get-size screen)
        message "This is a squirrel's nut"
        col (- (/ (size 0) 2)
               (/ (count message) 2))
        row (/ (size 1) 2)]
    (lanterna.screen/put-string screen col row message))
  (lanterna.screen/redraw screen)
  (lanterna.screen/get-key-blocking screen)
  (lanterna.screen/clear screen)
  (lanterna.screen/put-string screen 0 0 (apply str card-1))
  (lanterna.screen/put-string screen 0 1 (apply str card-2))
  (lanterna.screen/redraw screen)
    (loop [perception (lanterna.screen/get-key-blocking screen)]
      (if (= perception common-symbol)
        (lanterna.screen/put-string screen 0 2 "Correct")
        (recur (lanterna.screen/get-key-blocking screen))))
  (lanterna.screen/redraw screen)
  (lanterna.screen/get-key-blocking screen)
  (let [escape-key (lanterna.constants/key-codes)]
    (lanterna.screen/put-string screen 4 6 escape-key))
  (lanterna.screen/stop screen))
