(ns lonely-dobble.centered-text
  (:require [lanterna.screen])
  (:require [clojure.string :as str])
  (:gen-class))

;;(def vowels (set (map str "aeiou")))
(def screen (lanterna.screen/get-screen :text))


;; 3: other

(defn -main
  "I don't do a whole lot ... yet."
  [& args]  
  (lanterna.screen/start screen)
  (let [size (lanterna.screen/get-size screen)
        message "This is a squirrel's work"
        col (- (/ (size 0) 2)
               (/ (count message) 2))
        row (/ (size 1) 2)]
    (lanterna.screen/put-string screen col row message))
  (lanterna.screen/redraw screen)
  (lanterna.screen/get-key-blocking screen)
  (lanterna.screen/stop screen))


