(ns lonely-dobble.core
  (:require [lanterna.screen]
            [clojure.java.io :as io]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce]
            [clojure.edn :as edn]
            [clojure.string  :as str]
            [lonely-dobble.wrap-string :as wrap-string])
  (:use [clojure.tools.trace]
        [clojure.set :only (difference)])
  (:gen-class))

(def screen (lanterna.screen/get-screen :text))

(def stats-file "/tmp/stats.edn")

(defn read-stats! []
  (if (.exists (io/file stats-file))
    (edn/read-string (slurp stats-file))
    ()))

(def stats-atom (atom (read-stats!)))

(defn average! []
  (if (not-empty (deref stats-atom))
    (float (/ (reduce + (deref stats-atom))(count (deref stats-atom))))
    0))

(defn write-stats! [stats]
  (spit stats-file (pr-str stats)))

(defn welcome-screen []
  (let [text (str/split-lines (wrap-string/wrap-paragraphs 45 (slurp "src/lonely_dobble/welcome_text.txt")))]
    (doseq [[index  line] (map-indexed vector text)]
      (let [size (lanterna.screen/get-size screen)
            height-of-text (count text)
            horizontal-margin (if
                                  (> (size 0) 180)
                                80
                                25)
            vertical-margin (/ (- (size 1) height-of-text) 2)
            row (+ vertical-margin index)]
        (lanterna.screen/put-string screen horizontal-margin row line))))
  (lanterna.screen/redraw screen)
  (lanterna.screen/get-key-blocking screen)
  (lanterna.screen/clear screen))

(def alphabets (set "abcdefghijklmnopqrstuvwxyz"))

(defn take-random [n alphabets]
  (take n (shuffle alphabets)))

(defn generate-cards []
  (let [card-1 (take-random 5 alphabets)
        common-symbol (rand-nth card-1) 
        card-2 (->> card-1
                    (difference alphabets)
                    (take-random 4)
                    (cons common-symbol))]
    {:card-1 card-1 :common-symbol common-symbol :card-2 card-2})) 


(defn await-correct-input [correct]
  (loop [perception (lanterna.screen/get-key-blocking screen)]
    (cond
      (= perception correct) true
      (= perception :escape) false
      :else (recur (lanterna.screen/get-key-blocking screen)))))

(defn game-screen []
  (loop [cards (generate-cards)
         latency nil
         start-time (coerce/to-long (time/now))]
    (lanterna.screen/put-string screen 0 0 (apply str (:card-1 cards)))
    (lanterna.screen/put-string screen 0 1 (apply str (:card-2 cards)))
    (lanterna.screen/move-cursor screen 0 25)
    (let [size (lanterna.screen/get-size screen)
          row (- (size 1) 2)]
      (lanterna.screen/put-string screen 0 row (str "Time: " latency " milliseconds"))
      (lanterna.screen/put-string screen
                                  0
                                  (+ row 1)
                                  (str "Average-time: " (average!) " milliseconds")))
    (lanterna.screen/redraw screen)
    (if (await-correct-input (:common-symbol cards))
      (let [current-time (coerce/to-long (time/now))
            previous-latency (- current-time start-time)]
        (->> previous-latency
             (swap! stats-atom conj)
             (write-stats!))
        (recur (generate-cards)
               (- current-time start-time)
               current-time)))))



(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (lanterna.screen/start screen)
  (welcome-screen)
  (game-screen)
  (lanterna.screen/stop screen))
