(ns lonely-dobble.edn
  (:require [clj-time.core :as time])
  (:require [clj-time.coerce :as c])
  (:gen-class))

(def timestamp (c/to-long (time/now)))


(defrecord timer [start end])

(def game-time [(timer. timestamp
             (c/to-long (time/now)))])

(defn gaming ([game-begins game-ends]
              (->timer game-begins game-ends)))

(def game1 (gaming timestamp (c/to-long (time/now))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (spit "/tmp/writer.edn" (pr-str gaming) :append true))

