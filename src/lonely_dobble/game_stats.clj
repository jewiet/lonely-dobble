(ns lonely-dobble.game-stats
  (:require [lanterna.screen]
            [clojure.java.io :as io]
            [clj-time.core :as time]
            [clojure.edn :as edn]
            [clj-time.coerce :as coerce]
            [clojure.string  :as str]
            [clj-wrap-indent.core :as wrap])
  (:use [clojure.tools.trace]
        [clojure.set :only (difference)])
  (:gen-class))

(def screen (lanterna.screen/get-screen :text))

(def stats-file "/tmp/stats.edn")

(defn read-stats! []
  (if (.exists (io/file stats-file))
    (edn/read-string (slurp stats-file))
    ()))

(def stats-atom (atom (read-stats!)))

(defn average! []
  (if (not-empty (deref stats-atom))
    (float (/ (reduce + (deref stats-atom))(count (deref stats-atom))))
    0))

(defn write-stats! [stats]
  (spit stats-file (pr-str stats)))

(defn welcome-screen []
  (let [size (lanterna.screen/get-size screen)
        message (slurp "src/lonely_dobble/welcome_text.txt")
        col (- (/ (size 0) 2)
               (/ (count message) 2))
        row (/ (size 1) 2)]
    (lanterna.screen/put-string screen col row message))
  (lanterna.screen/redraw screen)
  (lanterna.screen/get-key-blocking screen)
  (lanterna.screen/clear screen))

(def symbols (set "abcdefghijklmnopqrstuvwxyz"))

(defn take-random [n symbols]
  (take n (shuffle  symbols)))

(defn generate-cards []
  (let [card-1 (take-random 5 symbols)
        common-symbol (rand-nth card-1) 
        card-2 (->> card-1
               (difference symbols)
               (take-random 4)
               (cons common-symbol))]
    {:card-1 card-1 :common-symbol common-symbol :card-2 card-2})) 

(defn await-correct-input [correct]
  (loop [perception (lanterna.screen/get-key-blocking screen)]
    (cond
      (= perception correct) true
      (= perception :escape) false
      :else (recur (lanterna.screen/get-key-blocking screen)))))

(defn game-screen []
  (loop [cards (generate-cards)
         latency nil
         start-time (coerce/to-long (time/now))]
    (lanterna.screen/put-string screen 0 0 (apply str (:card-1 cards)))
    (lanterna.screen/put-string screen 0 1 (apply str (:card-2 cards)))
    (lanterna.screen/move-cursor screen 34 4)
    (lanterna.screen/put-string screen 4 4 (str latency))
    (lanterna.screen/put-string screen
                                0
                                (+ row 1)
                                (str "Average-time: " (average!) " milliseconds"))
    (lanterna.screen/redraw screen)
    (if (await-correct-input (:common-symbol cards))
      (let [current-time (coerce/to-long (time/now))
            previous-latency (- current-time start-time)]
        (->> previous-latency
             (swap! stats-atom conj)
             (write-stats!))
        (recur (generate-cards)
               (- current-time start-time)
               current-time)))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (lanterna.screen/start screen)
  (welcome-screen)
  (game-screen)
  (lanterna.screen/stop screen))
