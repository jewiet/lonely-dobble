(ns lonely-dobble.log-reader
  (:require [clojure.edn :as edn])
  (:gen-class))

(def entries (->> "/tmp/writer.log"
                  (slurp)
                  (clojure.string/split-lines)
                  (map edn/read-string)))
(def num-entries (->> "/tmp/writer.log"
                      (slurp)
                      (clojure.string/split-lines)
                      (count)))

(def sum (reduce + entries))

(def average (/ sum (count entries)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println num-entries)
  (println (long average)))

