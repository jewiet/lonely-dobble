(ns lonely-dobble.log-writer
  (:require [clj-time.core :as t])
  (:require [clj-time.coerce :as c])
  (:gen-class))

(def timestamp (c/to-long (t/now)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (spit "/tmp/stats.edn"  
        (str timestamp "\n") 
        :append true))

