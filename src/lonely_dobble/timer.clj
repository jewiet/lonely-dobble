(ns lonely-dobble.timer
  (:require [clj-time.core :as t])
  (:require [clj-time.local :as l])
  (:require [clj-time.coerce :as c])
  (:gen-class))

(def program-start-time (c/to-long (t/now)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println program-start-time)
  (read-line)
  (println (c/to-long (t/now)))
  (println (- (c/to-long (t/now)) program-start-time)))

