(ns lonely-dobble.user-input
  (:require [clj-time.core :as time])
  (:require [clj-time.coerce :as c])
  (:require [clojure.edn :as edn])
  (:require [lanterna.terminal :as t])
  (:gen-class))


(def term (t/get-terminal :text))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (t/start term)
  (let [character (t/get-key-blocking term)]
    (t/stop term)
    (println character)))


