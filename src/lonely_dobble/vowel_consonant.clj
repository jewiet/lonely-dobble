(ns lonely-dobble.vowel-consonant
  (:require [lanterna.screen])
  (:require [clojure.string :as str])
  (:gen-class))

;;(def vowels (set (map str "aeiou")))
(def vowels (set "aeiou"))

(def screen (lanterna.screen/get-screen :text))

(def consonants (set "bcdfghjklmnpqrstvwxyz"))

;;(def consonants (set (map str "bcdfghjklmnpqrstvwxyz")))

;; make the vowel? and consonant? predicates work on strings
;; use read-line to get input

(defn vowel? 
  [ch]
  (not (nil? (vowels ch))))

(defn consonant? 
  "This is a predicate for a consonant"
  [ch]
  (not (nil? (consonants ch))))

;; Input: anything
;; Output: one of :vowel :consonant :what?

(defn classify
  "a function to classify characters into vowels and consonants"
  [ch]
  (cond
    (vowel? ch) :vowel
    (consonant? ch) :consonant
    :else :what?))

;; Plan:
;; Probably have a cond
;; 1: vowel?
;; 2: a-z? 2 options: ASCII TABLE (int \c) or REGEX
;; 3: other

(defn -main
  "I don't do a whole lot ... yet."
  [& args]  
  (lanterna.screen/start screen)
  (loop [character (lanterna.screen/get-key-blocking screen) 
         line 0]
    (lanterna.screen/put-string screen 0 line (str (classify character)))
    (lanterna.screen/redraw screen)
    (recur (lanterna.screen/get-key-blocking screen) (inc line)))
  (lanterna.screen/get-size screen)
  (lanterna.screen/stop screen))


