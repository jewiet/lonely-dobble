(ns lonely-dobble.wrap-string
  (:require [clojure.string  :as str])
  (:require [clojure.tools.trace :refer [deftrace]]))

(defn iterator [length lines word]
    (let
        [line (first lines)
         fits (< (+ (count word)
                    (count line)
                    1)
                 length)
         previous-lines (rest lines)]
      (cond
        (nil? line) [word]
        fits (cons (str line " " word) previous-lines)
        :else (->> previous-lines
                     (cons line)
                     (cons word)))))


(defn group-lines [length words]
  (->> words 
       (reduce (partial iterator length) []) 
       (reverse)))

(defn paragraphs [input]
  (str/split input #"\n\n"))

(defn wrap [length paragraph]
  (->> paragraph
       (#(str/split % #" "))
       (group-lines length)
       (str/join "\n")))

(defn wrap-paragraphs [length input]
  (->> input
       (paragraphs)
       (map (partial wrap length))
       (str/join "\n\n")))
